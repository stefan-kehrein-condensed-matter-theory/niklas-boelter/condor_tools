#!/usr/bin/env python3
import argparse
from collections import defaultdict
import json
import subprocess

parser = argparse.ArgumentParser(description='Show Cluster Usage.')

parser.add_argument('-v', '--verbose', action='store_true',
                                    help='Show running jobs as well as nodes')
args = parser.parse_args()

def progress_bar(x, length=40):
    if x < 0.60:
        color = '\033[32m'
    elif x < 0.90:
        color = '\033[34m'
    else:
        color = '\033[31m'
    x = int(x * length)
    return '[' + color + '|'*x + '\033[0m' + ' '*(length-x) + ']'

condor_status = subprocess.Popen('condor_status -json -attributes Machine,TotalSlotMemory,TotalSlotCpus,SlotType,StartJobs', shell=True, stdout=subprocess.PIPE).stdout

slot_memory = {}
slot_memory_usage = defaultdict(lambda: 0)

slot_cpus = {}
slot_cpus_usage = defaultdict(lambda: 0)

slot_slots = defaultdict(lambda: 0)

slot_startjobs = defaultdict(lambda: None)

for slot in json.load(condor_status):
    machine = slot['Machine']
    memory = slot['TotalSlotMemory']
    cpus = slot['TotalSlotCpus']
    startJobs = slot['StartJobs']

    if slot['SlotType'] == 'Partitionable':
        slot_memory[machine] = memory
        slot_cpus[machine] = cpus
        slot_startjobs[machine] = startJobs
    else:
        slot_memory_usage[machine] += memory
        slot_cpus_usage[machine] += cpus
        slot_slots[machine] += 1

if args.verbose:
    for k in slot_memory.keys():
        print(k.replace('.local', '').rjust(15), ' Memory:', progress_bar(slot_memory_usage[k]/slot_memory[k]), '  CPU:', progress_bar(slot_cpus_usage[k]/slot_cpus[k]), '   Dynamical Slots:', slot_slots[k])
        if slot_startjobs[k] != True:
            print("       └ StartJobs = False")
    print()
count = 0
utilization = 0.0
for k in slot_memory.keys():
    if slot_startjobs[k]:
        utilization += max(slot_memory_usage[k]/slot_memory[k], slot_cpus_usage[k]/slot_cpus[k])
        count += 1
utilization = utilization / count

print("Cluster Load: ", progress_bar(utilization, 100), str(round(utilization * 100, 1)) + "%")
print("Total Memory: ", progress_bar(sum(slot_memory_usage.values()) / sum(slot_memory.values()), 100), sum(slot_memory_usage.values())//1024, "GiB /", sum(slot_memory.values())//1024, "GiB")
print("Total CPUs:   ", progress_bar(sum(slot_cpus_usage.values()) / sum(slot_cpus.values()), 100), sum(slot_cpus_usage.values()), "/", sum(slot_cpus.values()))
print("Total Dynamical Slots: ", sum(slot_slots.values()))
